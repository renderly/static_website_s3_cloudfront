variable "cloudfront_domain" { type = string }
variable "cloudfront_zone_id" { type = string }
variable "domain_name" { type = string }