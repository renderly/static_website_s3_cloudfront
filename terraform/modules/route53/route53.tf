############################################
# ROUTE 53
############################################

// Try this for existing bucket in Route-53:
// https://www.terraform.io/docs/providers/aws/r/route53_record.html

data "aws_route53_zone" "main" {
  name = var.domain_name
}

resource "aws_route53_record" "main-a-record" {
  zone_id = data.aws_route53_zone.main.zone_id
  name = var.domain_name
  type = "A"
  alias {
    name    = var.cloudfront_domain
    zone_id = var.cloudfront_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "main-c-name" {
  zone_id = data.aws_route53_zone.main.zone_id
  name = "www"
  type = "CNAME"
  ttl = "300"
  records = [ var.domain_name ]
}

resource "aws_route53_record" "main-a-wildcard-record" {
  zone_id = data.aws_route53_zone.main.zone_id
  name = "*.${var.domain_name}"
  type = "A"
  alias {
    name    = var.cloudfront_domain
    zone_id = var.cloudfront_zone_id
    evaluate_target_health = false
  }
}
