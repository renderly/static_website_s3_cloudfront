variable "domain_cert_arn" { type = string }
variable "domain_name" { type = string }
variable "origin_id" { type = string }
variable "website_endpoint" { type = string }
