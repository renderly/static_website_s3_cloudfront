############################################
# CLOUDFRONT
############################################

resource "aws_cloudfront_distribution" "s3_distribution" {

  aliases = [ var.domain_name, "*.${var.domain_name}" ]
  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"
  comment             = "Managed by Terraform"

  origin {
    domain_name = var.website_endpoint
    origin_id   = var.origin_id
    custom_origin_config {
      http_port               = 80
      https_port              = 443
      origin_protocol_policy  = "http-only"
      origin_ssl_protocols    = ["TLSv1.2"]
    }
  }

  default_cache_behavior {
    target_origin_id = var.origin_id
    viewer_protocol_policy = "redirect-to-https"

    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    compress               = true
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    smooth_streaming       = true

    forwarded_values {
      query_string = true

      cookies {
        forward = "none"
      }
    }

  }

  price_class = "PriceClass_100"  // Cheapest for US/Canada https://aws.amazon.com/cloudfront/pricing/

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    //cloudfront_default_certificate = true
    acm_certificate_arn       = var.domain_cert_arn
    minimum_protocol_version  = "TLSv1"
    ssl_support_method        = "sni-only"
  }
}
