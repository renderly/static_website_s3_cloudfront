############################################
# S3 Static Website Files
############################################

resource "aws_s3_bucket_object" "index" {
  bucket       = var.s3_bucket_static_id
  key          = "index.html"
  source       = "./html/index.html"
  content_type = "text/html"
  acl          = "public-read"
  # etag makes the file update when it changes; see https://stackoverflow.com/questions/56107258/terraform-upload-file-to-s3-on-every-apply
  etag = filemd5("./html/index.html")
}
resource "aws_s3_bucket_object" "error" {
  bucket       = var.s3_bucket_static_id
  key          = "error.html"
  source       = "./html/error.html"
  content_type = "text/html"
  acl          = "public-read"
  # etag makes the file update when it changes; see https://stackoverflow.com/questions/56107258/terraform-upload-file-to-s3-on-every-apply
  etag = filemd5("./html/error.html")
}

/*
terraform mv 
*/
