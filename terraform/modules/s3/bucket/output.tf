output "static_id" {
    value = aws_s3_bucket.static.id
}
output "website_endpoint" {
    value = aws_s3_bucket.static.website_endpoint
}
output "website_domain" {
    value = aws_s3_bucket.static.website_domain
}
output "regional_domain_name" {
    value = aws_s3_bucket.static.bucket_regional_domain_name
}
