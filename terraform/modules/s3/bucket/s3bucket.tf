############################################
# S3 Bucket
############################################

resource "aws_s3_bucket" "static" {
  bucket        = var.artifacts_bucket
  acl           = "public-read"  // "private"  // "public-read"  // try "private" because each file is individually set as '--acl public-read'
  force_destroy = true

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    Name = "Website"
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    //expose_headers = ["ETag"]
    max_age_seconds = 3000
  }
}
