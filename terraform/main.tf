############################################
# S3 Bucket
############################################

module "s3_bucket" {
  source = "./modules/s3/bucket"
  artifacts_bucket = var.artifacts_bucket
}



# ############################################
# # S3 Static Website Files
# ############################################

module "s3_static_website_files" {
  source = "./modules/s3/files"
  s3_bucket_static_id = module.s3_bucket.static_id
}



############################################
# CLOUDFRONT
############################################

module "cloudfront" {
  source = "./modules/cloudfront"
  domain_cert_arn = var.domain_cert_arn
  domain_name = var.domain_name
  origin_id = var.s3_origin_id
  website_endpoint  = module.s3_bucket.website_endpoint
}


############################################
# ROUTE 53
############################################

module "route53" {
  source = "./modules/route53"
  cloudfront_domain = module.cloudfront.domain_name
  cloudfront_zone_id = module.cloudfront.hosted_zone_id
  domain_name = var.domain_name
}
