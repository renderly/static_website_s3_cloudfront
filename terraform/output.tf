#### Output vars between modules
output "s3_bucket_static_id" {
    value = module.s3_bucket.static_id
}
output "s3_bucket_website_endpoint" {
    value = module.s3_bucket.website_endpoint
}



#### Base outputs
output "aws_s3_webside_domain" {
 value = [ module.s3_bucket.website_domain ]
}

output "aws_s3_webside_domain_manual" {
 value = [ join("", [ var.artifacts_bucket, ".s3-website-us-west-2.amazonaws.com" ]) ]
}

output "aws_s3_webside_domain_regional" {
 value = [ module.s3_bucket.regional_domain_name ]
}

output "aws_s3_website_endpoint" {
 value = [ module.s3_bucket.website_endpoint ]
}

output "aws_acm_certificate_arn" {
 value = [ var.domain_cert_arn ]
}
