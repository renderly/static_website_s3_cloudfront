############################################
# AWS PROVIDERS
# Ideas from https://github.com/rogerwelin/serverless-website-tutorial/blob/master/terraform/lambda-apigw-iam/main.tf
############################################
terraform {
  required_version = ">= 0.15"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.37"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region                  = "us-west-2"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "renderly"
}



// https://www.terraform.io/docs/providers/aws/r/acm_certificate.html
// https://www.intricatecloud.io/2018/04/creating-your-serverless-website-in-terraform-part-2/ 
// ^ Manage the certificate with terraform, but create it manually and then use terraform import to import the resource AFTER its been approved and created.
////terraform import aws_acm_certificate.cert arn:aws:acm:us-west-2:344262896900:certificate/958e612f-cabe-43f5-930c-286e4112d58d

// https://www.terraform.io/docs/providers/aws/d/acm_certificate.html
//# Find a certificate issued by (not imported into) ACM
# data "aws_acm_certificate" "ssl_cert" {
#   domain      = "*.req.to"
#   types       = ["ISSUED"]
#   most_recent = true
# }
