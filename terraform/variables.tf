variable "domain_name" {
  type = string
	default = "EXAMPLE.COM"
}

variable "domain_cert_arn" {
  type = string
  default = "arn:aws:acm:us-east-1:123EXAMPLE123:certificate/123-123-123-123-example-cert"
}

variable "s3_origin_id" {
  type = string
	default = "EXAMPLE_COM_website_origin"
}

variable "artifacts_bucket" {
  type = string
  default = "EXAMPLE-COM-static"
}

variable "path_to_static_html" {
  type = string
  default = "./html"  // relative to this root directory
}
